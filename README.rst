============
payplug-java
============
payplug-java is a library to create payment with payplug.
This library is based on integration document Version 0.5 
that you can find on PayPLug website: https://www.payplug.fr

===========
Get Started
===========
- Put your payplug private key in youPrivateKey file
  attention to the format.
- Change the baseUrl in Example class with your baseUrl
- run it
