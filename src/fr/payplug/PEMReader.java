package fr.payplug;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PEMReader {
    public static final String PRIVATE_PKCS1_MARKER = "-----BEGIN RSA PRIVATE KEY-----";
    public static final String PRIVATE_PKCS8_MARKER = "-----BEGIN PRIVATE KEY-----";

    private static final String BEGIN_MARKER = "-----BEGIN ";

    private InputStream stream;
    private byte[] derBytes;
    private String beginMarker;

    public PEMReader(InputStream inStream) throws IOException {
        stream = inStream;
        readFile();
    }

    public PEMReader(byte[] buffer) throws IOException {
        this(new ByteArrayInputStream(buffer));
    }
    public byte[] getDerBytes() {
        return derBytes;
    }

    public String getBeginMarker() {
        return beginMarker;
    }

    protected void readFile() throws IOException {
        String  line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        try {
            while ((line = reader.readLine()) != null)
            {
                if (line.indexOf(BEGIN_MARKER) != -1)
                {
                    beginMarker = line.trim();
                    String endMarker = beginMarker.replace("BEGIN", "END");
                    derBytes = readBytes(reader, endMarker);
                    return;
                }
            }
            throw new IOException("Invalid PEM file: no begin marker");
        } finally {
            reader.close();
        }
    }
    private byte[] readBytes(BufferedReader reader, String endMarker) throws IOException
    {
        String          line = null;
        StringBuffer    buf = new StringBuffer();

        while ((line = reader.readLine()) != null)
        {
            if (line.indexOf(endMarker) != -1) {

                return Base64.decode(buf.toString());
            }

            buf.append(line.trim());        
        }

        throw new IOException("Invalid PEM file: No end marker");
    }    
}
