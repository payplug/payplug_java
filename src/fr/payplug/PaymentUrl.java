/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 PayPlug
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package fr.payplug;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.LinkedHashMap;
import java.util.Map;

import net.oauth.signature.pem.PKCS1EncodedKeySpec;

/**
 * PaymentUrl generate payment url like discribe in payplug module e-commerce documentation
 * For more information visit : https://www.payplug.fr
 * @author Stephane Planquart
 *
 */
public class PaymentUrl {
    protected Map<String, String> params;
    protected PrivateKey privateKey;
    protected String baseUrl;
    /**
     * @param baseUrl
     */
    public PaymentUrl(String baseUrl) {
        params=new LinkedHashMap<String, String>();
        this.baseUrl = baseUrl;
    }
    /**
     * @param name
     * @param value
     * example : 
     * PaymentUrl paymentUrl = new PaymentUrl("...");
     * paymentUrl.addParam("amount","100");    //amount = 1EUR
     */
    public void addParam(String name, String value){
        params.put(name, value);
    }

    public void loadPrivateKey(byte[]  encodedPrivateKey) 
            throws InvalidKeySpecException, NoSuchAlgorithmException, IOException{
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        KeySpec keySpec = null;
        PEMReader pr = new PEMReader(encodedPrivateKey);
        if(PEMReader.PRIVATE_PKCS1_MARKER.equals(pr.getBeginMarker())){
            keySpec = (new PKCS1EncodedKeySpec(pr.getDerBytes())).getKeySpec();
        }else if(PEMReader.PRIVATE_PKCS8_MARKER.equals(pr.getBeginMarker())){
        	keySpec = new PKCS8EncodedKeySpec(pr.getDerBytes());
        }
        this.privateKey = keyFactory.generatePrivate(keySpec);
    }

    public String getUrl() 
            throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, SignatureException{
        String url_params = Url.toQueryString(params);
        String signature = this._getSignature(url_params);
        Map<String, String> dataSign=new LinkedHashMap<String, String>();
        dataSign.put("data", Base64.encode(url_params.getBytes()));
        dataSign.put("sign", signature);
        return baseUrl + '?' + Url.toQueryString(dataSign);
    }

    protected String _getSignature(String message)
            throws NoSuchAlgorithmException, InvalidKeySpecException, 
            InvalidKeyException, SignatureException{
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(privateKey);
        signature.update(message.getBytes());
        byte [] signatureBytes = signature.sign();
        String sign = Base64.encode(signatureBytes);
        return sign;
    }
}
