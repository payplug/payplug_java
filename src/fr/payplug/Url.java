package fr.payplug;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;

public class Url {
    public static String toQueryString(Map<?,?> data) throws UnsupportedEncodingException {
         StringBuffer queryString = new StringBuffer();

         for (Entry<?,?> pair : data.entrySet()) {
             queryString.append(URLEncoder.encode((String)pair.getKey(), "UTF-8" ) + "=" );
             queryString.append(URLEncoder.encode((String)pair.getValue(), "UTF-8" ) + "&" );
         }

         if (queryString.length () > 0) {
             queryString.deleteCharAt ( queryString.length () - 1 );
         }

         return queryString.toString ();
    }
}
