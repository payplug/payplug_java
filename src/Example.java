/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 PayPlug
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;

import fr.payplug.PaymentUrl;

public class Example {

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("This example show how to use PaymentUrl class!");
        try {
            //change it with your baseUrl (see in payplug documentation)
            String baseUrl = "https://www.payplug.fr/p/b/3FlVSA8nEeOVpSIACiKh1Q==";	

            //load privatekey data
            //change it with YOUR payplug private key (see in payplug documentation)
            byte[] keyBytes = Files.readAllBytes(Paths.get("./yourPrivateKey")); 

            PaymentUrl payplug = new PaymentUrl(baseUrl);
            payplug.loadPrivateKey(keyBytes);
            payplug.addParam("amount", "142");	//amount in cents : 1,42€ here
            payplug.addParam("currency","EUR");
            payplug.addParam("ipn_url","http://test.test.com/ipn.php");
            payplug.addParam("return_url","http://www.google.com/");
            payplug.addParam("email","joen.do@test.com");
            payplug.addParam("firstname","Joen");
            payplug.addParam("lastname","Do");
            payplug.addParam("customer","42");
            payplug.addParam("order","198");
            payplug.addParam("custom_datas","product_id=2|ip_address=88.161.117.16");
            String url = payplug.getUrl();
            System.out.println("url : " + url);

        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SignatureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("end");
    }
}
